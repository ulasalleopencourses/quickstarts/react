# React

El objetivo de este curso es aprender de las funcionalidades básicas de React y desarrollar proyectos simples para poner en práctica lo aprendido. Además, compararemos react con otros frameworks, evaluando sus ventajas y desventajas.

# Herramientas

- Node.js 12.18
- Visual Studio Code
- React 17

# Videos

**Unidad 1: Primeros pasos en React**
- Instalar herramientas para proyecto de  desarrollo de React.js
- Setup del editor


**Unidad 2: Mini juego para comprender conceptos**
- Explorar paquete de desarrollo de principiante
- Uso de funciones
- Desarrollo de componente interactivo
- Explorar conceptos más complejos al mejorar el juego

**Unidad 3: Conclusiones finales**
- Comparación con otros Frameworks
- Hablar de mercado laboral de React

# Tutorial de referencia

Documentación: https://es.reactjs.org/tutorial/tutorial.html#before-we-start-the-tutorial

Demo final: https://codepen.io/gaearon/pen/oWWQNa?editors=0010


# Unidad 1

**Primeros pasos en React**

Tutorial: https://youtu.be/10dwYoNDLI8

Documentación de referencia: https://es.reactjs.org/tutorial/tutorial.html#before-we-start-the-tutorial


**Enlaces de descarga**

Node: https://nodejs.org/es/download/

VsCode: https://code.visualstudio.com/download


# Unidad 2

**Mini juego para comprender conceptos**

Tutorial: https://youtu.be/4qL8-3A3vt8

Demo final: https://codepen.io/gaearon/pen/oWWQNa?editors=0010


**Enlaces de descarga**

Extensión de chrome para React: https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi/related?hl=en


# Unidad 3
**Conclusiones finales**

Video: https://youtu.be/-WXhJfyWxcg


# Autor
Sara Díaz Oporto
sdiazo@ulasalle.edu.pe



